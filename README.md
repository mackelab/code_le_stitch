# Fork of pop_spike_dyn, to be extended for stitching

Moved development to github:
https://github.com/mackelab/le_stitch/



Contact for pop_spike_dyn
* Lars Buesing (primary contact), lars@stat.columbia.edu 

Contact for stitching additions:
* Marcel Nonnenmacher, marcel.nonnenmacher@caesar.de 